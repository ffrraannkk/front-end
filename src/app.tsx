import * as React from 'react';

import { SignUpPage } from './pages';

export class App extends React.Component {
  public render() {
    return (
      <SignUpPage />
    );
  }
}
