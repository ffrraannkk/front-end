export const Styles = {
  signUp: (theme: any) => {
    return {
      blockquote: {
        borderLeft: 'grey solid 2px',
        color: 'grey',
        margin: 0,
        padding: '0 20px'

      },
      button: {

      },
      buttonWrapper: {
        display: 'flex',
        justifyContent: 'space-between',
        maxWidth: '210px'
      },
      centerFlex: {
        alignItems: 'center',
        display: 'flex',
        justifyContent: 'center'
      },
      container: {
        alignItems: 'center',
        display: 'flex',
        height: '100%',
        justifyContent: 'center'
      },
      error: {
        backgroundColor: 'red',
        color: 'white',
        height: '30px',
        margin: '16px'
      },
      form: {

      },
      image: {
        margin: 'auto',
        maxWidth: '25px'
      },
      infoIcon: {
        '&:hover': {
          color: '#222'
        },
        color: 'grey',
        cursor: 'pointer',
        fontSize: '8px',
        left: '-5px',
        top: '50%',
        transform: 'translateY(-50%)',
        transition: 'color .5s ease-out'
      },
      linearProgress: {
        width: '80%'
      },
      maxSize: {
        maxWidth: '210px'
      },
      modal: {
        left: '50%',
        outline: 'none',
        top: '50%',
        transform: 'translate(-50%, -50%)'
      },
      paper: {
        padding: '16px'
      },
      passwordViewer: {
        '&:hover': {
          color: '#222'
        },
        color: 'grey',
        cursor: 'pointer',
        right: 0,
        top: '50%',
        transform: 'translate(-16px, -50%) scale(1)',
        transition: 'color .5s ease-out'
      },
      progressIcon: {
        right: '16px',
        top: 0,
        transition: 'color .5s ease-out'
      },
      rose: {
        color: '#ffc1e3'
      },
      signIn: {
        fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif;',
        fontSize: '12px'
      },
      text: {
        fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif;',
        fontSize: '0.875rem',
        margin: 0
      },
      textField: {
        '&[aria-invalid="false"]': {
          borderLeftColor: 'salmon'
        },
        '&[aria-invalid="true"]': {
          borderLeftColor: 'palegreen'
        },
        borderLeft: '3px solid',
        transition: 'border-color .5s ease-out'
      }
    }
  }
}
