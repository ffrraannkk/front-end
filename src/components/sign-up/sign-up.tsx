import * as React from 'react';
import * as zxcvbn from 'zxcvbn';
import { wait } from '../../helpers';

import {
  Button, // https://material-ui.com/demos/buttons/
  CircularProgress,
  Grid,
  LinearProgress,
  Modal,
  Paper,
  Snackbar,
  TextField, // https://material-ui.com/demos/text-fields/
  withStyles
} from '@material-ui/core';

import { Styles } from './styles';

class SignUp extends React.Component<any, any> {
  public strength = {
    0: {
      color: 'grey',
      icon: 'meh-rolling-eyes'
    },
    25: {
      color: 'indianred',
      icon: 'smile-beam'
    },
    50: {
      color: '#ffae6a',
      icon: 'laugh-beam'
    },
    75: {
      color: '#409999',
      icon: 'kiss-beam'
    },
    100: {
      color: 'palegreen',
      icon: 'grin-hearts'
    }
  }

  public state = {
    email: '',
    emailValid: false,
    isLoading: false,
    password: '',
    passwordHint: {
      suggestions: [],
      warning: ''
    },
    passwordStrength: 0,
    passwordValid: false,
    showPasswordHint: false,
    signInError: false,
    username: '',
    usernameValid: false,
    viewPassword: false
  }

  public render() {
    const { classes } = this.props;

    const {
      email,
      emailValid,
      isLoading,
      password,
      passwordHint,
      passwordStrength,
      passwordValid,
      showPasswordHint,
      signInError,
      username,
      usernameValid,
      viewPassword
    } = this.state;

    const ButtonContent = isLoading
      ? <CircularProgress className={classes.rose} size={20} />
      : 'Sign Up';

    const SignInError = signInError
      ? <p className={classes.text}>An unexpected error has occurred <i className="fas fa-exclamation-circle"/></p>
      : null;

    const disableButton = !usernameValid || !passwordValid || !emailValid;

    return (
      <div className={classes.container}>
        <Paper className={classes.paper}>
          <div className={classes.buttonWrapper}>
            <a target="_blank" href="https://mynewknow.com">
              <img
                className={classes.image}
                src="https://pbs.twimg.com/profile_images/909487474606432256/RVO-qCPZ_400x400.jpg"
              />
            </a>
            <div className={classes.signIn}>
              Already a member? <a href="https://app.mynewknow.com/login">Sign in</a>
            </div>
          </div>
          <form className={classes.form}>
            <Grid container={true} direction="column" spacing={16}>
              <Grid className={classes.centerFlex} item={true}>
                <TextField
                  id="username"
                  label="  Username"
                  className={classes.textField}
                  value={username}
                  aria-invalid={usernameValid}
                  autoComplete="username"
                  onChange={this.handleChange('username')}
                  margin="normal"
                  required={true}
                />
              </Grid>
              <Grid className={classes.centerFlex} style={{ position: 'relative' }} item={true}>
                <TextField
                  id="password"
                  label="  Password"
                  className={classes.textField}
                  value={password}
                  type={viewPassword ? 'text' : 'password'}
                  autoComplete="current-password"
                  aria-invalid={passwordValid}
                  onChange={this.handleChange('password')}
                  required={true}
                  InputProps={{
                    endAdornment: (
                      <i
                        hidden={!password.length}
                        className={`fas fa-${viewPassword ? 'eye-slash' : 'eye'} ${classes.passwordViewer}`}
                        onMouseDown={this.onViewPassword()}
                        onMouseUp={this.onHidePassword()}
                        onMouseLeave={this.onHidePassword()}
                        style={{ position: 'absolute' }}
                      />
                    )
                  }}
                />
              </Grid>
              <Grid style={{ position: 'relative' }} item={true}>
                <i
                  hidden={(!passwordHint.warning && !passwordHint.suggestions.length)}
                  onClick={this.showPasswordHint()}
                  style={{ position: 'absolute' }}
                  className={`far fa-question-circle ${classes.infoIcon} fa-xs"`}
                />
                <LinearProgress
                  className={`${classes.linearProgress}`}
                  variant="determinate"
                  value={passwordStrength}
                />
                <i
                  style={{ position: 'absolute', color: this.strength[passwordStrength].color }}
                  className={`fas fa-${this.strength[passwordStrength].icon} ${classes.progressIcon}`}
                />
              </Grid>
              <Grid className={classes.centerFlex} item={true}>
                <TextField
                  id="email"
                  label="Email"
                  className={classes.textField}
                  value={email}
                  aria-invalid={emailValid}
                  autoComplete="email"
                  onChange={this.handleChange('email')}
                  margin="normal"
                  required={true}
                />
              </Grid>
              <Grid item={true}>
                <div className={classes.buttonWrapper}>
                  <Button
                    disabled={disableButton}
                    variant="contained"
                    color="primary"
                    className={classes.button}
                    onClick={this.onSaveClicked()}
                  >
                    {ButtonContent}
                  </Button>
                </div>
              </Grid>
              <Grid item={true}>
                <div className={`${classes.maxSize} ${classes.text}`}>
                  By signing up, you are agreeing to our <a target="_blank" href="https://www.mynewknow.com/terms-and-conditions">Terms and Conditions</a>.
                </div>
              </Grid>
            </Grid>
          </form>
        </Paper>
        <Modal onClose={this.hidePasswordHint()} open={showPasswordHint}>
          <Paper
            style={{ flexWrap: 'wrap', position: 'absolute' }}
            className={`${classes.modal} ${classes.text} ${classes.centerFlex} ${classes.paper}`}
          >
            <h1 style={{ fontSize: '14px', width: '100%' }}>Password Suggestions</h1>
            <div style={{ width: '100%' }}>
              <p hidden={!passwordHint.warning}>Warning</p>
              <blockquote className={classes.blockquote}>
                {passwordHint.warning}
              </blockquote>
              <p hidden={!passwordHint.suggestions.length}>Suggestions</p>
              {passwordHint.suggestions.length
                && passwordHint.suggestions.map((suggestion, i) => {
                  return <blockquote className={classes.blockquote} key={i}>{suggestion}</blockquote>;
                })
                || null
              }
            </div>
          </Paper>
        </Modal>
        <Snackbar
          className={classes.error}
          open={!!signInError}
          onClose={this.onCloseSnackbar()}
          autoHideDuration={1000}
        >
          {SignInError}
        </Snackbar>
      </div>
    )
  }

  private onViewPassword = () => (event: any) => {
    this.setState((prevState: any) => ({ viewPassword: !prevState.viewPassword }));
  }

  private onHidePassword = () => (event: any) => {
    this.setState({ viewPassword: false });
  }

  private showPasswordHint = () => (event: any) => {
    this.setState({ showPasswordHint: true });
  }

  private hidePasswordHint = () => (event: any) => {
    this.setState({ showPasswordHint: false });
  }

  private handleChange = (fieldName: string) => (event: any) => {
    // Typescript won't let me do dynamic programming?
    // const capitalizedField: string = fieldName.charAt(0).toUpperCase() + fieldName.slice(1)
    // const error = this[`validate${capitalizedField}`](event.target.value);
    let error = this.state[`${fieldName}Error`];
    if (fieldName === 'username') {
      error = this.validateUsername(event.target.value);
    } else if (fieldName === 'password') {
      error = this.validatePassword(event.target.value);
    } else {
      error = this.validateEmail(event.target.value);
    }

    this.setState({
      [`${fieldName}Valid`]: error,
      signInError: false,
      [fieldName]: event.target.value,
    }, () => {
      const { score, feedback: suggestions } = zxcvbn(this.state.password);
      this.setState({
        passwordHint: suggestions,
        passwordStrength: score * 25
      });
    });
  }

  private validateEmail = (text: string): boolean => { // tslint:disable-line
    // 'g' flag to get substring in match
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/g;
    return !!text.match(re);
  }

  // tslint:disable-next-line
  private validatePassword = (text: string): boolean => {
    const textTooShort = text.length < 8;
    if (textTooShort) {
      return false;
    }

    if (this.state.passwordStrength < 75) {
      return false;
    }

    return true;
  }

  // tslint:disable-next-line
  private validateUsername = (text: string): boolean => {
    const textTooShort = text.length < 3;
    if (textTooShort) {
      return false;
    }

    return true;
  }

  private onCloseSnackbar = () => (event: any) => {
    this.setState({ signInError: false });
  }

  private onSaveClicked = () => async (event: any) => {
    this.setState({ isLoading: true, signInError: false });
    await wait(3000);
    this.setState({ isLoading: false, signInError: true });
  }
}

export default withStyles(Styles.signUp)(SignUp);
