import * as React from 'react';

export default class HomePage extends React.Component<any, any> {
  public render() {
    const { user } = this.props;

    return (
      <div>
        <h1>{`Welcome, ${user.username}`}</h1>
      </div>
    )
  }
}
