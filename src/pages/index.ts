import { HomePage } from './home-page';
import { SignUpPage } from './sign-up-page';

export {
  HomePage,
  SignUpPage
}
